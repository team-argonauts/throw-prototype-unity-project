﻿using UnityEngine;
using UnityEngine.InputSystem;


[RequireComponent(typeof(PlayerInputManager))]
public class CustomPlayerInputManager : MonoBehaviour
{
    private PlayerInputManager inputManager;

    public GameObject[] playerPrefabs = null;

    public Vector3 spawnCenter = Vector3.zero;
    public float spawnRangeX = 10.0f;


    private void Awake()
    {
        inputManager = GetComponent<PlayerInputManager>();
    }

    private static Vector3 GetSpawnPosition(Vector3 spawnCenter, float spawnRangeX, int playerIndex, int maxPlayerCount)
    {
        Vector3 spawnSeparation = Vector3.right * (spawnRangeX / (maxPlayerCount - 1));
        Vector3 spawnPosition = spawnCenter + (playerIndex * spawnSeparation) - (Vector3.right * spawnRangeX * 0.5f);

        return spawnPosition;
    }

    private void OnPlayerJoined(PlayerInput playerInput)
    {
        PlayerSpawner playerSpawner = playerInput.gameObject.GetComponent<PlayerSpawner>();
        if (playerSpawner == null)
        {
            Debug.LogError("Player Input Object does not have a PlayerSpawner!", playerInput.gameObject);
            return;
        }

        playerSpawner.playerPrefab = playerPrefabs[playerInput.playerIndex];
        Vector3 spawnPosition = GetSpawnPosition(spawnCenter, spawnRangeX, playerInput.playerIndex, inputManager.maxPlayerCount);
        playerSpawner.spawnPoint = spawnPosition;
        playerSpawner.SpawnPlayer();
    }
}
