﻿using System.Collections;
using UnityEngine;
using UltEvents;

[RequireComponent(typeof(Rigidbody))]
public class SlamAbility : MonoBehaviour
{
    public float slamForce = 50.0f;
    public float slamDelay = 0.1f;
    
    private Rigidbody rb;

    public UltEvent OnBeforeSlam;
    public UltEvent OnSlam;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public IEnumerator DoSlam()
    {
        OnBeforeSlam?.Invoke();

        yield return new WaitForSeconds(slamDelay);

        OnSlam?.Invoke();
        rb.AddForce(Vector3.down * slamForce, ForceMode.VelocityChange);
    }

    private void OnAltFire()
    {
        StartCoroutine(DoSlam());
    }
}