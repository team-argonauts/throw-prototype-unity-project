﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSlowDownController : MonoBehaviour
{
    private static GlobalSlowDownController instance = null;
    private static List<Transform> slowDownTargets = new List<Transform>();

    public float slowDownDistanceThreshold = 2.0f;
    [Range(0.0f, 1.0f)]
    public float slowDownStrength = 0.75f;
    public float slowDownTime = 1.0f;
    [Range(0.0f, 1.0f)]
    public float slowDownSlope = 0.8f;

    private bool currentInRange = false;
    private bool previousInRange = false;


    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one GlobalSlowDownController!", gameObject);
            return;
        }

        instance = this;
    }


    private IEnumerator DoSlowDown()
    {
        MyTimer slowDownTimer = new MyTimer();
        slowDownTimer.Start(slowDownTime);

        while (!slowDownTimer.stopped && currentInRange)
        {
            float slowDownValue = slowDownStrength * TweenScaleFunctions.UpDownPower(slowDownTimer.progress, 1.0f - slowDownSlope);
            float newTimeScale = 1.0f - slowDownValue;
            Time.timeScale = newTimeScale;
            yield return null;
        }

        Time.timeScale = 1.0f;
    }

    private static bool TargetsWithinMinDist(Transform[] targets, float minDist)
    {
        for (int i = 0; i < targets.Length - 1; ++i)
            for (int j = i + 1; j < targets.Length; ++j)
            {
                float dist = Vector3.Distance(targets[i].position, targets[j].position);
                if (dist < minDist)
                    return true;
            }

        return false;
    }
    private void Update()
    {
        currentInRange = TargetsWithinMinDist(slowDownTargets.ToArray(), slowDownDistanceThreshold);

        if (!previousInRange && currentInRange)
            StartCoroutine(DoSlowDown());

        previousInRange = currentInRange;
    }


    public static void AddSlowDownTarget(Transform target)
    {
        if (!slowDownTargets.Contains(target))
            slowDownTargets.Add(target);
    }
    public static void RemoveSlowDownTarget(Transform target)
    {
        if (slowDownTargets.Contains(target))
            slowDownTargets.Remove(target);
    }
}
