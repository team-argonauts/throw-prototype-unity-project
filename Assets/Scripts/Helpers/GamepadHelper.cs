﻿using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;


public static class GamepadHelper
{
    public static Gamepad GetGamepadFromPlayerInput(PlayerInput input)
    {
        return Gamepad.all.FirstOrDefault(g => input.devices.Any(d => d.deviceId == g.deviceId));
    }

    public static Gamepad GetGamepadFromGameObject(GameObject go)
    {
        PlayerInput input = go.GetComponentInParent<PlayerInput>();
        if (input == null)
            return null;

        Gamepad foundGamepad = GetGamepadFromPlayerInput(input);

        return foundGamepad;
    }
}