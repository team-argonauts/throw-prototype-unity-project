﻿using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;


public static class CollisionHelper
{
    public static Vector3 GetAverageCollisionPoint(Collision collision)
    {
        Vector3 averageCollisionPoint = Vector3.zero;

        for (int i = 0; i < collision.contactCount; ++i)
            averageCollisionPoint += collision.GetContact(i).point;

        return averageCollisionPoint / collision.contactCount;
    }

    public static Vector3 GetAverageCollisionNormal(Collision collision)
    {
        Vector3 averageCollisionNormal = Vector3.zero;

        for (int i = 0; i < collision.contactCount; ++i)
            averageCollisionNormal += collision.GetContact(i).normal;

        return averageCollisionNormal / collision.contactCount;
    }
}