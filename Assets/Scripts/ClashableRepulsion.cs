﻿using System;
using UnityEngine;
using UltEvents;


[RequireComponent(typeof(Rigidbody))]
public class ClashableRepulsion : MonoBehaviour
{
    private Rigidbody rb;

    public float clashForce = 15.0f;
    private Vector2 delayedClashForce = Vector2.zero;

    [Serializable]
    public class ClashEvent : UltEvent<Vector3> { }
    public ClashEvent OnClashEvent;



    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }


    private void DoClashableRepulsion(Collider other)
    {
        Collider myCollider = rb.GetComponentInChildren<Collider>();
        if (!myCollider)
            return;

        Vector3 clashPosition = (myCollider.bounds.center + other.bounds.center) / 2.0f;
        OnClashEvent?.Invoke(clashPosition);

        Vector2 forceDir = (myCollider.bounds.center - other.bounds.center).normalized;
        delayedClashForce += clashForce * forceDir;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Clashable"))
        {
            DoClashableRepulsion(other);
        }
    }


    /// <summary> FixedUpdate used to delay force application so both triggers have time to see the clash. </summary>
    private void FixedUpdate()
    {
        rb.AddForce(delayedClashForce, ForceMode.VelocityChange);
        delayedClashForce = Vector2.zero;
    }
}
