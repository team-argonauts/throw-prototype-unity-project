﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;


// General Rule: Use Translate() rather than set the position to allow for multiple camera effects at once without drifting.

[RequireComponent(typeof(Camera))]
[DisallowMultipleComponent]
public class GlobalCameraController : MonoBehaviour
{
    private static GlobalCameraController instance = null;
    private Camera attachedCamera;

    private List<Transform> followTargets = new List<Transform>();
    private Vector3 followVelocity;

    public Vector3 offset;
    public float smoothTime;

    public float minZoom = 40.0f;
    public float maxZoom = 10.0f;
    public float zoomLimiter = 50.0f;

    public Bounds movementBounds;


    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one CameraController in the Scene!", this);
            return;
        }

        attachedCamera = GetComponent<Camera>();
        if (attachedCamera != Camera.main)
        {
            Debug.LogError("CameraController not attached to MainCamera!", this);
            return;
        }

        instance = this;
    }


    private Bounds GetTargetBounds()
    {
        Bounds targetBounds = new Bounds(followTargets[0].position, Vector3.zero);
        for (int i = 0; i < followTargets.Count; ++i)
            targetBounds.Encapsulate(followTargets[i].position);

        return targetBounds;
    }

    private Vector3 GetCenterPoint()
    {
        return GetTargetBounds().center;
    }


    private Vector3 GetCameraMovement()
    {
        Vector3 centerPoint = GetCenterPoint();
        Vector3 clampedCenter = new Vector3(Mathf.Clamp(centerPoint.x, movementBounds.center.x - movementBounds.extents.x, movementBounds.center.x + movementBounds.extents.x), 
                                            Mathf.Clamp(centerPoint.y, movementBounds.center.y - movementBounds.extents.y, movementBounds.center.y + movementBounds.extents.y), 
                                            Mathf.Clamp(centerPoint.z, movementBounds.center.z - movementBounds.extents.z, movementBounds.center.z + movementBounds.extents.z));
        Vector3 desiredPosition = clampedCenter + offset;
        Vector3 desiredVector = desiredPosition - transform.position;

        return Vector3.SmoothDamp(Vector3.zero, desiredVector, ref followVelocity, smoothTime * Time.deltaTime);
    }


    private float GetGreatestTargetDistance()
    {
        Bounds targetBounds = GetTargetBounds();
        return Mathf.Max(targetBounds.size.x, targetBounds.size.y);
    }

    private float GetFollowZoom()
    {
        float desiredZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestTargetDistance() / zoomLimiter);
        return Mathf.Lerp(attachedCamera.fieldOfView, desiredZoom, Time.deltaTime);
    }


    private void LateUpdate()
    {
        if (followTargets.Count <= 0)
            return;

        transform.Translate(GetCameraMovement());
        attachedCamera.fieldOfView = GetFollowZoom();
    }

    public static void AddFollowTarget(Transform followTarget)
    {
        if (!instance.followTargets.Contains(followTarget))
            instance.followTargets.Add(followTarget);
    }

    public static void RemoveFollowTarget(Transform followTarget)
    {
        if (instance.followTargets.Contains(followTarget))
            instance.followTargets.Remove(followTarget);
    }


    private IEnumerator ShakeScreen(float shakeRadius, float shakeDuration)
    {
        MyTimer shakeTimer = new MyTimer();
        shakeTimer.Start(shakeDuration);

        Vector3 previousShake = Vector3.zero;

        while (!shakeTimer.stopped)
        {
            Vector3 currentShake = Random.insideUnitSphere * shakeRadius;
            Vector3 movement = currentShake - previousShake;
            previousShake = currentShake;
            transform.Translate(movement);
            yield return null;
        }

        transform.Translate(-previousShake);
    }

    public static void StartScreenShake(CameraShakeParameter shakeParameter)
    {
        instance.StartCoroutine(instance.ShakeScreen(shakeParameter.radius, shakeParameter.duration));
    }


    private IEnumerator DoSineBounce(float bounceStrength, Vector3 bounceDirection, float bounceDuration)
    {
        MyTimer bounceTimer = new MyTimer();
        bounceTimer.Start(bounceDuration);

        float previousBounceValue = 0.0f;
        bounceDirection.Normalize();

        do
        {
            yield return null;

            float currentBounceValue = Mathf.Sin(bounceTimer.progress * Mathf.PI);
            float bounceDiff = currentBounceValue - previousBounceValue;
            Vector3 movement = bounceDiff * bounceStrength * bounceDirection;
            transform.Translate(movement);

            previousBounceValue = currentBounceValue;
        } 
        while (!bounceTimer.stopped);
    }


    public static void SineBounce(CameraBounceParameter bounceParameter)
    {
        instance.StartCoroutine(instance.DoSineBounce(bounceParameter.strength, bounceParameter.direction, bounceParameter.duration));
    }
}