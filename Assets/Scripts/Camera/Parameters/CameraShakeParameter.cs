﻿using UnityEngine;


[CreateAssetMenu(menuName = "Throw Thyself/Camera Effects/Camera Shake Parameter")]
public class CameraShakeParameter : ScriptableObject
{
    [SerializeField]
    private float m_Radius = 0.0f;
    [SerializeField]
    private float m_Duration = 0.0f;

    public float radius { get => m_Radius; }
    public float duration { get => m_Duration; }
}