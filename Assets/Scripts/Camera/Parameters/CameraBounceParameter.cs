﻿using UnityEngine;


[CreateAssetMenu(menuName = "Throw Thyself/Camera Effects/Camera Bounce Parameter")]
public class CameraBounceParameter : ScriptableObject
{
    [SerializeField]
    private float m_Strength = 0.0f;
    [SerializeField]
    private Vector3 m_Direction = Vector3.zero;
    [SerializeField]
    private float m_Duration = 0.0f;

    public float strength { get => m_Strength; }
    public Vector3 direction { get => m_Direction; }
    public float duration { get => m_Duration; }
}