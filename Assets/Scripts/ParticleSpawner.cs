﻿using UnityEngine;


public class ParticleSpawner
{
    public static void Spawn(GameObject particlePrefab, Vector3 position)
    {
        Object.Instantiate(particlePrefab, position, particlePrefab.transform.rotation);
    }

    public static void Spawn(GameObject particlePrefab, Vector3 position, Quaternion rotation)
    {
        Object.Instantiate(particlePrefab, position, rotation);
    }

    public static void Spawn(GameObject particlePrefab, Transform transform)
    {
        Object.Instantiate(particlePrefab, transform);
    }
}
