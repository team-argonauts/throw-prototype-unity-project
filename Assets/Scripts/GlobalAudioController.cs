﻿using UnityEngine;


public class GlobalAudioController
{
    public static void PlayClipAtPoint(AudioClip clip, Vector3 position)
    {
        AudioSource.PlayClipAtPoint(clip, position);
    }
    public static void PlayClipAtPoint(AudioClip clip, Transform transform)
    {
        AudioSource.PlayClipAtPoint(clip, transform.position);
    }
}