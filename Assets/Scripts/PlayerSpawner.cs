﻿using UnityEngine;
using UnityEngine.InputSystem;


[RequireComponent(typeof(PlayerInput))]
public class PlayerSpawner : MonoBehaviour
{
    public GameObject playerPrefab = null;
    private GameObject currentPlayer = null;

    public Vector3 spawnPoint = Vector3.zero;


    public void SpawnPlayer()
    {
        if (currentPlayer != null)
            Destroy(currentPlayer);

        currentPlayer = Instantiate(playerPrefab, spawnPoint, playerPrefab.transform.rotation);
        currentPlayer.transform.SetParent(transform);
    }

    private void OnSpawn()
    {
        SpawnPlayer();
    }
}
