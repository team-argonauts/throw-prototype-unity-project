﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.HID;

public class GlobalRumbleController : MonoBehaviour
{
    private static GlobalRumbleController instance;
    private static Dictionary<GameObject, Gamepad> gamepads = new Dictionary<GameObject, Gamepad>();


    private struct RumblePair
    {
        public RumblePair(float lowA, float highA)
        {
            this.lowA = lowA;
            this.highA = highA;
        }

        public float lowA;
        public float highA;
    }

    private Dictionary<Gamepad, RumblePair> highestRumbles = new Dictionary<Gamepad, RumblePair>();



    private static void ValidateInstance()
    {
        if (instance != null)
        {
            return;
        }

        GameObject root = new GameObject { name = "GlobalRumbleController", hideFlags = HideFlags.HideAndDontSave };
        instance = root.AddComponent<GlobalRumbleController>();
        instance.hideFlags = HideFlags.HideAndDontSave;
    }


    private void Update()
    {
        foreach (Gamepad gamepad in highestRumbles.Keys.ToArray())
        {
            gamepad.SetMotorSpeeds(highestRumbles[gamepad].lowA, highestRumbles[gamepad].highA);
            highestRumbles[gamepad] = new RumblePair(0.0f, 0.0f);
        }
    }


    private void TrySetRumble(Gamepad gamepad, float lowA, float highA)
    {
        if (!highestRumbles.ContainsKey(gamepad))
        {
            highestRumbles.Add(gamepad, new RumblePair(lowA, highA));
            return;
        }

        RumblePair currentRumble = highestRumbles[gamepad];
        highestRumbles[gamepad] = new RumblePair(Mathf.Max(lowA, currentRumble.lowA), Mathf.Max(highA, currentRumble.highA));
    }

    
    private IEnumerator DoRumblePattern(Gamepad gamepad, float lowA, float highA, Func<float, float> rumblePattern, float duration)
    {
        if (gamepad == null)
            yield break;


        MyTimer rumbleTimer = new MyTimer();
        rumbleTimer.Start(duration);
        WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

        while (!rumbleTimer.stopped)
        {
            float rumbleValue = rumblePattern.Invoke(rumbleTimer.progress);
            TrySetRumble(gamepad, lowA * rumbleValue, highA * rumbleValue);
            yield return waitForEndOfFrame;
        }

        TrySetRumble(gamepad, 0.0f, 0.0f);
    }
    public static void StartRumblePattern(GameObject rumbleObject, RumblePatternParameter patternParameter)
    {
        ValidateInstance();
        Gamepad gamepad = GamepadHelper.GetGamepadFromGameObject(rumbleObject);
        instance.StartCoroutine(instance.DoRumblePattern(gamepad, patternParameter.lowA, patternParameter.highA, patternParameter.pattern, patternParameter.duration));
    }


    public static void SetScaledRumble(GameObject rumbleObject, float scalePercent, RumbleParameter rumbleParameter)
    {
        ValidateInstance();
        Gamepad gamepad = GamepadHelper.GetGamepadFromGameObject(rumbleObject);
        if (gamepad == null)
            return;

        instance.TrySetRumble(gamepad, rumbleParameter.lowA * scalePercent, rumbleParameter.highA * scalePercent);
    }
    public static void SetRumble(GameObject rumbleObject, RumbleParameter rumbleParameter)
    {
        ValidateInstance();
        Gamepad gamepad = GamepadHelper.GetGamepadFromGameObject(rumbleObject);
        if (gamepad == null)
            return;

        instance.TrySetRumble(gamepad, rumbleParameter.lowA, rumbleParameter.highA);
    }


    public void StopAllRumbling()
    {
        foreach (Gamepad gamepad in highestRumbles.Keys.ToArray())
        {
            gamepad.SetMotorSpeeds(0.0f, 0.0f);
        }
    }
    private void OnApplicationQuit()
    {
        StopAllRumbling();
    }
}