﻿using System;
using UnityEngine;


[CreateAssetMenu(menuName = "Throw Thyself/Rumble Effects/Rumble Parameter")]
public class RumbleParameter : ScriptableObject
{
    [SerializeField]
    protected float m_LowA;
    [SerializeField]
    protected float m_HighA;

    public float lowA { get => m_LowA; protected set => m_LowA = value; }
    public float highA { get => m_HighA; protected set => m_HighA = value; }
}