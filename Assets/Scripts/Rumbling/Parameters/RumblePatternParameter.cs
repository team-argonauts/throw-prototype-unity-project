﻿using System;
using UnityEngine;


[CreateAssetMenu(menuName = "Throw Thyself/Rumble Effects/Rumble Pattern Parameter")]
public class RumblePatternParameter : RumbleParameter
{
    [SerializeField]
    private Func<float, float> m_Pattern = TweenScaleFunctions.Linear;
    [SerializeField]
    private float m_Duration = 0.0f;

    public Func<float, float> pattern { get => m_Pattern; }
    public float duration { get => m_Duration; }
}