﻿using UnityEngine;
using UnityEngine.InputSystem;


public class RotateTowardsCursor : MonoBehaviour
{
    public Transform followTarget;
    private float currentAimAngle;


    private void Update()
    {
        if (followTarget == null)
            return;

        transform.position = followTarget.position;
        transform.rotation = Quaternion.Euler(0, 0, currentAimAngle);
    }

    private void OnMousePosition(InputValue value)
    {
        Vector2 targetScreenPoint = Camera.main.WorldToScreenPoint(followTarget.position);
        Vector2 aimDirection = (value.Get<Vector2>() - targetScreenPoint).normalized;

        if (aimDirection.magnitude <= 0)
            return;

        currentAimAngle = Mathf.Rad2Deg * Mathf.Atan2(aimDirection.y, aimDirection.x);
    }
    private void OnMove(InputValue value)
    {
        Vector2 aimDirection = value.Get<Vector2>();
        if (aimDirection.magnitude <= 0)
            return;

        currentAimAngle = Mathf.Rad2Deg * Mathf.Atan2(aimDirection.y, aimDirection.x);
    }
}
