﻿using UltEvents;
using UnityEngine;


public class PlayerExplosion : MonoBehaviour
{
    public bool exploded { get; private set; } = false;

    public float explosionForce = 2500.0f;
    public float explosionRadius = 2.0f;

    public Rigidbody[] rigidbodies;
    public CharacterJoint[] characterJoints;

    public UltEvent OnExplode;


    private Vector3 GetAveragePosition()
    {
        Vector3 avgPosition = Vector3.zero;

        foreach (Rigidbody rb in rigidbodies)
            avgPosition += rb.position;

        return avgPosition / rigidbodies.Length;
    }

    private void BreakJoints()
    {
        foreach (CharacterJoint cj in characterJoints)
            cj.breakForce = 0.0f;
    }

    private void AddExplosionForce()
    {
        Vector3 explosionPosition = GetAveragePosition();

        foreach (Rigidbody rb in rigidbodies)
            rb.AddExplosionForce(explosionForce, explosionPosition, explosionRadius);
    }

    public void DoExplode()
    {
        if (exploded)
            return;

        OnExplode?.Invoke();

        BreakJoints();
        AddExplosionForce();

        exploded = true;
    }
}
