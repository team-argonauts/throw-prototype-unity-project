﻿using UltEvents;
using UnityEngine;


public class ExplodePlayerOnCollision : MonoBehaviour
{
    public UltEvent OnExplode;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            PlayerExplosion otherExplosion = collision.gameObject.GetComponentInParent<PlayerExplosion>();
            if (otherExplosion == null || otherExplosion.exploded)
                return;

            OnExplode?.Invoke();
            otherExplosion.DoExplode();
        }
    }
}