﻿using System.Collections.Generic;
using UnityEngine;



public class VelocityTracker
{
    public int maxVelocityIterations = 5;
    public List<Vector2> previousPositions = new List<Vector2>();
    public Vector2 velocity
    {
        get
        {
            if (previousPositions.Count <= 1)
                return Vector2.zero;

            Vector2 totalVelocity = Vector2.zero;
            float velocityCount = previousPositions.Count - 1;
            for (int i = 0; i < velocityCount; ++i)
                totalVelocity += (previousPositions[i] - previousPositions[i + 1]);

            return totalVelocity / velocityCount;
        }
    }


    public VelocityTracker(int maxVelocityIterations)
    {
        this.maxVelocityIterations = maxVelocityIterations;
    }

    public void TrackPosition(Vector3 position)
    {
        previousPositions.Insert(0, position);

        if (previousPositions.Count > maxVelocityIterations)
            previousPositions.RemoveAt(maxVelocityIterations);
    }
}