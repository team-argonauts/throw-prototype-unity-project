﻿using System.Collections.Generic;
using UnityEngine;


public static class RenderController
{
    private static Color SetColorGreyscale(Color color, float greyscale)
    {
        greyscale = Mathf.Clamp(greyscale, 0.01f, 1.0f);
        
        float H, S;
        Color.RGBToHSV(color, out H, out S, out _);

        return Color.HSVToRGB(H, S, greyscale);
    }

    public static void SetRendererColorGreyscale(Renderer renderer, string colorName, float greyscale)
    {
        Material rendererMaterial = renderer.material;
        Color rendererColor = rendererMaterial.GetColor(colorName);
        Color greyscaledColor = SetColorGreyscale(rendererColor, greyscale);
        rendererMaterial.SetColor(colorName, greyscaledColor);
    }
}