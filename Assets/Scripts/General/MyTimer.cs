﻿using System;
using System.Collections.Generic;
using UnityEngine;



public class MyTimer
{
    public float duration;
    public bool stopped { get => time >= duration; }
    public float progress { get => duration == 0.0f ? 0.0f : time / duration; }


    public float startTime;
    public float time { 
        get
        {
            float actualTime = Time.time - startTime;
            float clampedTime = Mathf.Clamp(actualTime, 0.0f, duration);
            return clampedTime;
        }
    }


    public void Start(float duration)
    {
        this.duration = duration;
        startTime = Time.time;
    }
}