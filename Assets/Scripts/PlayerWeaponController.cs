﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UltEvents;



[RequireComponent(typeof(Rigidbody))]
public class PlayerWeaponController : MonoBehaviour
{
    [HideInInspector]
    public bool active = true;


    [HideInInspector]
    public Rigidbody controlBody;
    public Rigidbody rootBody;
    public Collider weaponCollider;


    [Header("Throwing")]
    public float throwForce = 1000.0f;
    public float throwRampup = 4.0f;
    public float throwHoldTimeMax = 0.5f;
    [HideInInspector]
    public bool charging { get; private set; } = false;
    private MyTimer throwTimer = new MyTimer();


    [Header("Strafing")]
    public float strafeForce = 0.15f;
    private Vector2 currentAimDirection;


    [Serializable] public class ProgressEvent : UltEvent<float> {}
    [Header("Events")]
    public UltEvent OnActivate;
    public UltEvent OnDeactivate;
    public UltEvent OnChargeStart;
    public ProgressEvent OnChargeUpdate;
    public UltEvent OnChargeEnd;



    public void Activate()
    {
        OnActivate?.Invoke();
        active = true;
    }
    private void SetupSelfCollisionIgnoring()
    {
        foreach (Collider c in rootBody.GetComponentsInChildren<Collider>())
            Physics.IgnoreCollision(c, weaponCollider);
        Physics.IgnoreCollision(rootBody.GetComponent<Collider>(), weaponCollider);
    }
    private void Awake()
    {
        Activate();
        SetupSelfCollisionIgnoring();
        controlBody = GetComponent<Rigidbody>();
    }


    public void Deactivate()
    {
        OnDeactivate?.Invoke();
        active = false;
    }
    private void OnDestroy()
    {
        Deactivate();
    }


    private Vector2 GetStrafeForce()
    {
        return currentAimDirection * strafeForce;
    }
    private void OnStrafeUpdate()
    {
        if (!active)
            return;

        controlBody.AddForce(GetStrafeForce(), ForceMode.VelocityChange);
    }
    private void FixedUpdate()
    {
        OnStrafeUpdate();
    }


    private IEnumerator DoChargeUpdate()
    {
        while (charging && active)
        {
            float rampedThrowValue = GetRampedThrowValue();
            OnChargeUpdate?.Invoke(rampedThrowValue);

            yield return null;
        }
        yield break;
    }
    private void DoChargeStart()
    {
        if (!active)
            return;

        OnChargeStart?.Invoke();

        throwTimer.Start(throwHoldTimeMax);
        charging = true;

        StartCoroutine(DoChargeUpdate());
    }
    private void OnFireDown()
    {
        DoChargeStart();
    }


    private float GetRampedThrowValue()
    {
        float rampedThrowValue = Mathf.Pow(throwTimer.progress, throwRampup);
        return rampedThrowValue;
    }
    private Vector2 GetThrowForce()
    {
        return currentAimDirection * throwForce * GetRampedThrowValue();
    }
    private void Throw()
    {
        controlBody.AddForce(GetThrowForce(), ForceMode.VelocityChange);
    }
    private void DoChargeEnd()
    {
        if (!active)
            return;

        OnChargeEnd?.Invoke();

        Throw();
        charging = false;
    }
    private void OnFireUp()
    {
        DoChargeEnd();
    }


    private void OnMousePosition(InputValue value)
    {
        Vector2 controlBodyScreenPoint = Camera.main.WorldToScreenPoint(controlBody.position);
        Vector2 aimDirection = (value.Get<Vector2>() - controlBodyScreenPoint).normalized;
        if (aimDirection.magnitude > 0)
            currentAimDirection = aimDirection;
    }
    private void OnMove(InputValue value)
    {
        Vector2 aimDirection = value.Get<Vector2>().normalized;
        if (aimDirection.magnitude > 0)
            currentAimDirection = aimDirection;
    }
}
