﻿using System.Collections.Generic;
using UnityEngine;
using UltEvents;


[RequireComponent(typeof(Rigidbody))]
public class WallGrabber : MonoBehaviour
{
    private List<FixedJoint> grabJoints = new List<FixedJoint>();
    public bool grabbing { get => grabJoints.Count > 0; }
    public float velocityThreshold = 0.15f;

    private const int velocityIterations = 5;
    private VelocityTracker velocityTracker = new VelocityTracker(velocityIterations);
    private Rigidbody controlBody;

    public UltEvent OnGrab;
    public UltEvent OnDetach;


    
    private void Awake()
    {
        controlBody = GetComponent<Rigidbody>();
    }


    private void Grab()
    {
        if (grabJoints.Count > 0)
            Detach();

        OnGrab?.Invoke();
        grabJoints.Add(controlBody.gameObject.AddComponent<FixedJoint>());
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Grabbable"))
            return;

        Vector3 collisionNormal = CollisionHelper.GetAverageCollisionNormal(collision);
        float relativeVelocity = Mathf.Abs(Vector2.Dot(collisionNormal, velocityTracker.velocity));
        if (relativeVelocity < velocityThreshold)
            return;

        Grab();
    }


    public void Detach()
    {
        if (grabJoints.Count <= 0)
            return;

        OnDetach?.Invoke();

        foreach (FixedJoint joint in grabJoints)
            Destroy(joint);

        grabJoints.Clear();
    }


    private void FixedUpdate()
    {
        velocityTracker.TrackPosition(transform.position);
    }
}